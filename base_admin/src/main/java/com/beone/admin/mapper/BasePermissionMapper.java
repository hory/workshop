package com.beone.admin.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.base.SuperMapper;
import com.beone.admin.entity.BasePermission;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 运维数据_系统菜单功能权限表 Mapper 接口
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BasePermissionMapper extends SuperMapper<BasePermission> {

    /**
     * 根据用户ID 获取权限菜单信息
     * @param userId
     * @param  actionType  page  表示只显示页面权限   all  显示所有权限
     * @return
     */
    List<BasePermission> getPermissionsesByUserId(@Param("actionType") String actionType
            , @Param("userId") Integer userId);

    /**
     * 根据角色Id 获取权限菜单信息
     * @param roleId
     * @return
     */
    List<BasePermission> getPermissionsesByRoleId(@Param("roleId") Integer roleId);



    List<BasePermission> getPermissionPagination(RowBounds rowBounds
            , @Param("ew") Wrapper<BasePermission> wrapper);

    /**
     * 根据nodeId 获取权限信息
     * @param nodeId
     * @return
     */
    @Select("select a.node_id AS nodeId, a.node_name AS nameNode, a.node_type AS typeNode, a.node_level AS nodeLevel,"
            + "a.node_sort AS nodeSort, a.pid, a.node_url AS nodeUrl, a.is_index AS isIndex,a.node_code as nodeCode,"
            + "a.node_icon AS icon, p.node_name as parentName from base_permission a LEFT JOIN base_permission p ON a.pid=p.node_id"
            + " where a.node_id=#{nodeId}")
    BasePermission getPermissionByNodeId(@Param("nodeId") Integer nodeId);
}
