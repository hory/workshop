package com.beone.admin.mapper.provider;

import com.beone.admin.entity.BasePermissionRole;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * @title  角色权限关系 批量操作 Provider
 * @Author 覃球球
 * @Version 1.0 on 2018/1/26.
 * @Copyright 长笛龙吟
 */
public class BasePermissionRoleMapperProvider {
        //批量插入导购商品
        public String insertBatchPermissionRole(Map<String,List<BasePermissionRole>> map) {
            List<BasePermissionRole> goods = map.get("list");
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO base_permission_role(role_id, node_id) VALUES ");

            StringBuilder values = new StringBuilder();
            values.append("(#'{'list[{0}].roleId},#'{'list[{0}].nodeId})");

            MessageFormat messageFormat = new MessageFormat(values.toString());
            for(int i = 0 ;i < goods.size();i++) {
                sb.append(messageFormat.format(new Object[]{i}));
                if (i < goods.size() - 1) {
                    sb.append(",");
                }
            }
            return sb.toString();
        }
    }
