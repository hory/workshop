package com.beone.admin.utils.result;

/**
 * @Title 返回工具类
 * @Author 庞绪 on 2018/3/30.
 * @Copyright ©长沙科权
 */
public class ResultUtils {

    public static Result success() {
        return new Result(1, ResultCode.SUCCESS);
    }

    public static <T> Result success(T data, String token) {
        return new Result<>(1, ResultCode.SUCCESS.getKey(), data, token);
    }

    public static Result warn(ResultCode resultCode) {
        return new Result(0, resultCode);
    }

    public static Result warn(ResultCode resultCode, String msg) {
        Result result = new Result(0, resultCode);
        result.setMsg(msg);
        return result;
    }

}
