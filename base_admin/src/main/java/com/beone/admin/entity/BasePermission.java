package com.beone.admin.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.base.common.BaseModel;

import java.util.List;

/**
 * @Title 运维数据_系统菜单功能权限表 实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@TableName("base_permission")
public class BasePermission extends BaseModel {

    /**
     * 菜单ID
     */
	@TableId(value="node_id", type= IdType.AUTO)
	private Integer nodeId;
    /**
     * 菜单名称
     */
	@TableField("node_name")
	private String nameNode;
    /**
     * 菜单类型  T_node_type
     */
	@TableField("node_type" )
	private String typeNode;  // nodeName, nodeType 与 eayui-form 元素冲突; 不能进行表单验证; 所以取名 nameNode, typeNode
    /**
     * 菜单层级
     */
	@TableField("node_level")
	private Integer nodeLevel;
    /**
     * 菜单显示顺序
     */
	@TableField("node_sort")
	private Integer nodeSort;
    /**
     * 上级菜单ID
     */
	private Integer pid;
    /**
     * 菜单动作地址
     */
	@TableField("node_url")
	private String nodeUrl;
    /**
     * 是否显示 0-不显示 1-显示
     */
	@TableField("is_index")
	private Integer isIndex;

	@TableField("node_code")
	private String nodeCode;

	/**  图片 */
	@TableField("node_icon")
	private String icon;

	@TableField(exist = false)
	private String parentName;

	@TableField(exist = false)
	private Integer id;
	@TableField(exist = false)
	private Integer parentId;
	@TableField(exist = false)
	private String text;
	@TableField(exist = false)
	private String url;
	@TableField(exist = false)
	private String state;
	@TableField(exist = false)
	private List<BasePermission> children;
	@TableField(exist = false)
	private Boolean checked;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<BasePermission> getChildren() {
		return children;
	}

	public void setChildren(List<BasePermission> children) {
		this.children = children;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public String getNodeCode() {
		return nodeCode;
	}

	public void setNodeCode(String nodeCode) {
		this.nodeCode = nodeCode;
	}

	public Integer getNodeId() {
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	public String getNameNode() {
		return nameNode;
	}

	public void setNameNode(String nameNode) {
		this.nameNode = nameNode;
	}

	public String getTypeNode() {
		return typeNode;
	}

	public void setTypeNode(String typeNode) {
		this.typeNode = typeNode;
	}

	public Integer getNodeLevel() {
		return nodeLevel;
	}

	public void setNodeLevel(Integer nodeLevel) {
		this.nodeLevel = nodeLevel;
	}

	public Integer getNodeSort() {
		return nodeSort;
	}

	public void setNodeSort(Integer nodeSort) {
		this.nodeSort = nodeSort;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getNodeUrl() {
		return nodeUrl;
	}

	public void setNodeUrl(String nodeUrl) {
		this.nodeUrl = nodeUrl;
	}

	public Integer getIsIndex() {
		return isIndex;
	}

	public void setIsIndex(Integer isIndex) {
		this.isIndex = isIndex;
	}

	@Override
	public String toString() {
		return "BasePermission{" +
			", nodeId=" + nodeId +
			", nodeName=" + nameNode +
			", typeNode=" + typeNode +
			", nodeLevel=" + nodeLevel +
			", nodeSort=" + nodeSort +
			", pid=" + pid +
			", nodeUrl=" + nodeUrl +
			", isIndex=" + isIndex +
			"}";
	}
}
