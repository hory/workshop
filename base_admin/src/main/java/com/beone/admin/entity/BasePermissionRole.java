package com.beone.admin.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title 运维数据_角色对应的权限配置 实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@TableName("base_permission_role")
public class BasePermissionRole extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    @TableId("role_id")
	private Integer roleId;
    /**
     * 菜单ID
     */
	@TableField("node_id")
	private Integer nodeId;


	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getNodeId() {
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	@Override
	public String toString() {
		return "BasePermissionRole{" +
			", roleId=" + roleId +
			", nodeId=" + nodeId +
			"}";
	}
}
