package com.beone.admin.service;


import com.base.ISuperService;
import com.beone.admin.entity.BasePermissionRole;

import java.util.List;

/**
 * @Title 运维数据_角色对应的权限配置 服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BasePermissionRoleService extends ISuperService<BasePermissionRole> {

    /**
     * 根据角色Id, 删除角色与权限之间的关系
     * @param roleId
     */
    void deleteAccessRoleByRoleId(Integer roleId);


    /**
     * 批量插入角色与权限关系
     * @param items
     */
    void insertBatchAccessRole(List<BasePermissionRole> items);


    /**
     * 根据角色Id 获取角色对应的权限Id
     */
    List<BasePermissionRole> getPermissionIdByRoleId(Integer roleId);
}
