package com.beone.admin.common;

/**
 * @title Admin系统常量类
 * @Author 覃球球
 * @Version 1.0 on 2018/1/26.
 * @Copyright 长笛龙吟
 */
public interface AdminConstants {
    /**
     * 默认当前页码
     */
    String DEFAULT_PAGE_NUM = "1";
    /**
     * 默认每页最大能显示记录数
     */
    String PAGE_SIZE = "20";
    /**
     * 定时任务状态正常
     */
    int SCHEDULE_STATE_NORMAL = 0;
    /**
     * 定时任务状态暂停
     */
    int SCHEDULE_STATE_PAUSE = 1;
    /**
     * 可见
     */
    int ENABLED = 1;
    /**
     * 不可用
     */
    int DISABLED = 0;

    String DEFAULT_CHARACTER = "UTF-8";
}
