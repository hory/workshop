package com.beone.admin.quartz.job;

import com.beone.admin.service.SysLogService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @title  quartz job demo1 
 * @Author 覃球球
 * @Version 1.0 on 2018/10/26.
 * @Copyright 贝旺科技
 */
@Component
public class JobDemo1 extends BaseJob {

    @Autowired
    private SysLogService logService;

    @Override
    public void runTask() {
        System.out.println("JobDemo1：启动任务======================= ........ " + logService);
    }
}